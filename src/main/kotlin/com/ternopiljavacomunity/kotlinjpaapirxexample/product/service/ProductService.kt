package com.ternopiljavacomunity.kotlinjpaapirxexample.product.service

import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.request.CreatProductRequest
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.request.GetProductRequest
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.request.GetProductsRequest
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.response.CreateProductResponse
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.response.GetProductResponse
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.response.GetProductsResponse
import io.reactivex.Single

interface ProductService {

    fun createProduct(request: CreatProductRequest): Single<CreateProductResponse>

    fun getProducts(request: GetProductsRequest): Single<GetProductsResponse>

    fun getProduct(request: GetProductRequest): Single<GetProductResponse>
}