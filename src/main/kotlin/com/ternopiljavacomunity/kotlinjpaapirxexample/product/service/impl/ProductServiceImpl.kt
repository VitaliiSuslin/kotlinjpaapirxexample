package com.ternopiljavacomunity.kotlinjpaapirxexample.product.service.impl

import com.ternopiljavacomunity.kotlinjpaapirxexample.product.converter.toProductDto
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.database.enity.DbProduct
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.database.repository.ProductRepository
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.request.CreatProductRequest
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.request.GetProductRequest
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.request.GetProductsRequest
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.response.CreateProductResponse
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.response.GetProductResponse
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.response.GetProductsResponse
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.service.ProductService
import io.reactivex.Single
import javassist.NotFoundException
import org.springframework.stereotype.Service

@Service
class ProductServiceImpl
constructor(
        private val productRepository: ProductRepository
) : ProductService {

    override fun createProduct(request: CreatProductRequest): Single<CreateProductResponse> {
        return Single.fromCallable {
            productRepository.save(DbProduct(request.shopId, request.name, request.reference))
        }
                .map { CreateProductResponse(it.toProductDto()) }
    }

    override fun getProducts(request: GetProductsRequest): Single<GetProductsResponse> {
        return Single.fromCallable {
            productRepository.findAll()
        }
                .map { products -> GetProductsResponse(products.map { it.toProductDto() }.toMutableList()) }
    }

    @Throws(NotFoundException::class)
    override fun getProduct(request: GetProductRequest): Single<GetProductResponse> {
        return Single.fromCallable {
            productRepository.findById(request.productId)
        }
                .map {
                    GetProductResponse(it.orElseThrow {
                        NotFoundException("product by id:[ " + request.productId + " ] not found")
                    }.toProductDto())
                }
    }

//    fun test() {
//        Single.fromCallable { productRepository.findAll() }
//                .flatMap {
//                    Flowable.fromIterable(it)
//                            .flatMap {
//
//                            }
//                }
//
//        rxProductRepository.findAll()
//                .collect({ mutableListOf<DbProduct>() }, { list, item -> list.add(item) })
//                .doOnSuccess {
//
//                }
//    }
}
