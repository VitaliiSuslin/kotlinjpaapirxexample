package com.ternopiljavacomunity.kotlinjpaapirxexample.product.converter

import com.ternopiljavacomunity.kotlinjpaapirxexample.product.database.enity.DbProduct
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.response.dto.ProductDto

fun DbProduct.toProductDto():ProductDto{
    return ProductDto(shopId,name, reference, id)
}

