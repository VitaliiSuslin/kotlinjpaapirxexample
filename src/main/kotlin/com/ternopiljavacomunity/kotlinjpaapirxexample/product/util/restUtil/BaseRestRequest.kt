package com.ternopiljavacomunity.kotlinjpaapirxexample.product.util.restUtil

import com.fasterxml.jackson.annotation.JsonCreator

abstract class BaseRestRequest @JsonCreator constructor()