package com.ternopiljavacomunity.kotlinjpaapirxexample.product.database.enity

import javax.persistence.*

@Entity
@Table(name = "product")
data class DbProduct(

        @Column(name = "shop_id", nullable = false, unique = true)
        var shopId: Long,

        @Column(name = "name", nullable = false)
        var name: String,

        @Column(name = "reference",nullable = true)
        var reference: String? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0L
)