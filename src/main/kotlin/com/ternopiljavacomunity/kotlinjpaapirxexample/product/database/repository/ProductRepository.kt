package com.ternopiljavacomunity.kotlinjpaapirxexample.product.database.repository

import com.ternopiljavacomunity.kotlinjpaapirxexample.product.database.enity.DbProduct
import io.reactivex.Completable
import io.reactivex.Single
import org.springframework.data.domain.Example
import org.springframework.data.domain.ExampleMatcher
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.reactive.RxJava2CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ProductRepository : JpaRepository<DbProduct, Long>