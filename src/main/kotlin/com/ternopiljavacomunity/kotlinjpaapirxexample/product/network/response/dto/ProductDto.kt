package com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.response.dto

import com.fasterxml.jackson.annotation.JsonProperty
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.util.restUtil.BaseRestResponse

data class ProductDto(
        @JsonProperty("shop_id")
        var shopId: Long,

        @JsonProperty("name")
        var name: String,

        @JsonProperty("reference")
        var reference: String? = null,

        @JsonProperty("id")
        var id: Long
):BaseRestResponse()