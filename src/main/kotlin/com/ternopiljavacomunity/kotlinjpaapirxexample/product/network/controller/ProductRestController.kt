package com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.controller

import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.request.CreatProductRequest
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.request.GetProductRequest
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.request.GetProductsRequest
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.response.CreateProductResponse
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.response.GetProductResponse
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.response.GetProductsResponse
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.service.ProductService
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/api/product")
class ProductRestController
constructor(
        private val productService: ProductService
) {

    @PostMapping("/create")
    fun createProduct(
            @RequestBody
            request: CreatProductRequest): Single<ResponseEntity<CreateProductResponse>> {

        return productService.createProduct(request)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.single())
                .flatMap {
                    return@flatMap Single.just(ResponseEntity.ok(it))
                }
    }

    @GetMapping("/get_product&productId={id}")
    fun getProduct(
            @PathVariable
            id: Long): Single<ResponseEntity<GetProductResponse>> {

        return productService.getProduct(GetProductRequest(id))
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.single())
                .flatMap {
                    return@flatMap Single.just(ResponseEntity.ok(it))
                }
    }

    @PostMapping("/get_products")
    fun getProducts(
            @RequestBody
            request: GetProductsRequest
    ): Single<ResponseEntity<GetProductsResponse>> {

        return productService.getProducts(request)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.single())
                .flatMap {
                    return@flatMap Single.just(ResponseEntity.ok(it))
                }
    }
}