package com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.request

import com.fasterxml.jackson.annotation.JsonProperty
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.util.restUtil.BaseRestRequest

data class GetProductsRequest(
        @JsonProperty("shop_id")
        var shopId: Long,
        @JsonProperty("page_index")
        var pageIndex: Int,
        @JsonProperty("page_size")
        var pageSize: Int,
        @JsonProperty("sort_by")
        var sortBy: String
) : BaseRestRequest()