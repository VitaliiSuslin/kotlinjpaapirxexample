package com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.request

import com.fasterxml.jackson.annotation.JsonProperty
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.util.restUtil.BaseRestRequest

data class GetProductRequest(
        @JsonProperty("product_id")
        var productId: Long
):BaseRestRequest()