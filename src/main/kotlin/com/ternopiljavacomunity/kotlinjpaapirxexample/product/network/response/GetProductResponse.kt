package com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.response

import com.fasterxml.jackson.annotation.JsonProperty
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.response.dto.ProductDto
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.util.restUtil.BaseRestResponse

data class GetProductResponse(
        @JsonProperty("product")
        var product: ProductDto
): BaseRestResponse()