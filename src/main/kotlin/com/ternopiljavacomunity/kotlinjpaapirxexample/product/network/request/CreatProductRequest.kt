package com.ternopiljavacomunity.kotlinjpaapirxexample.product.network.request

import com.fasterxml.jackson.annotation.JsonProperty
import com.ternopiljavacomunity.kotlinjpaapirxexample.product.util.restUtil.BaseRestRequest


data class CreatProductRequest(
        @JsonProperty("shop_id")
        var shopId: Long,
        @JsonProperty("name")
        var name: String,
        @JsonProperty("reference")
        var reference: String
) : BaseRestRequest()