package com.ternopiljavacomunity.kotlinjpaapirxexample

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinJpaApiRxExampleApplication

fun main(args: Array<String>) {
    runApplication<KotlinJpaApiRxExampleApplication>(*args)
}
